let socket = io();
let roomName = "partida";

let center = document.createElement('center');
center.setAttribute('style', "margin-top: 200px")
let board = document.createElement('table');
let mensajes = document.getElementById('chatList');

let player1 = document.getElementById("player1").innerHTML;
let name = document.getElementById('username').innerHTML;
let player;
if (name === player1) {
    player = "player1";
}

let circleTurn = true;
//let playerTurn = "player1";
let selected = false;

socket.emit('join-room-game', name, roomName);

for (var i = 0; i < 3; i++) {
    var tr = document.createElement('tr');
    for (var j = 0; j < 3; j++) {

        var td = document.createElement('td');

        let button = document.createElement('button');
        button.disabled = true;
        button.classList.add("buttonImg");
        button.classList.add("row" + i);
        button.classList.add("col" + j);

        let img = document.createElement('img');
        img.id = "cell" + i + j;
        img.setAttribute("width", "100px");
        img.setAttribute("height", "100px");

        button.appendChild(img);

        button.onclick = function() {
            let row = (img.id).slice(4, 5);
            let col = (img.id).slice(-1);
            socket.emit('send-move', row, col, roomName, player);
            //place(img.id);
        }

        td.appendChild(button);

        if ((i + j) % 2 == 0) {
            td.setAttribute('class', 'cell whitecell');
            button.classList.add('whitecell');
            tr.appendChild(td);
        } else {
            td.setAttribute('class', 'cell redcell');
            button.classList.add('redcell');
            tr.appendChild(td);
        }
    }

    board.appendChild(tr);
}

center.appendChild(board);

board.setAttribute('cellspacing', '0');
document.body.appendChild(center);

function place(cellId) {
    let str;
    let row = (cellId).slice(4, 5);
    let col = (cellId).slice(-1);

    let cell = document.getElementById(cellId);

    if (cell.src !== "") {
        window.alert("Invalid movement");
    } else {
        if (circleTurn/* && (playerTurn === "player1")*/) {
            cell.src = "../img/circle.png";
            circleTurn = false;
            str = "circle";
            //playerTurn = "player2";
        } else/* if (!circleTurn && (playerTurn !== "player1"))*/ {
            cell.src = "../img/cross.png";
            circleTurn = true;
            str = "cross";
            //playerTurn = "player1";
        }
    }

    checkWin(str);
}

socket.on('move', (data) => {
    let buttonsArray = document.getElementsByClassName("buttonImg");
    let cellId = "cell" + data.row + "" + data.col;

    if (data.name !== player1) {
        for (let item of buttonsArray) {
            item.disabled = false;
        }
    }

    place(cellId);
});

socket.on('unblock-pieces', (data) => {
    let buttonsArray = document.getElementsByClassName("buttonImg");

    if (data.name !== player1) {
        for (let item of buttonsArray) {
            item.disabled = false;
        }
    }
});

function checkWin(str) {
    if (checkCol(0, str) || checkCol(1, str) || checkCol(2, str) || checkRow(0, str) || checkRow(1, str) || checkRow(2, str) || checkDiagonal1(str) || checkDiagonal2(str)) {
        window.alert(str + " win");
        window.location.href = "/play";
    }

    socket.emit('game-finish', name);
}

function checkRow(row, str) {
    let contador = 0;
    let buttonsInRow = document.getElementsByClassName("row" + row);

    for (let item of buttonsInRow) {
        if (item.firstElementChild.src.includes(str)) {
            contador++;
        }
    }

    return (contador === 3);
    //return (board.rows[row].cells[0].firstElementChild.src.includes(str) && board.rows[row].cells[1].firstElementChild.src.includes(str) && board.rows[row].cells[2].firstElementChild.src.includes(str));
}

function checkCol(col, str) {
    let contador = 0;
    let buttonsInRow = document.getElementsByClassName("col" + col);

    for (let item of buttonsInRow) {
        if (item.firstElementChild.src.includes(str)) {
            contador++;
        }
    }

    return (contador === 3);
    //return (board.rows[0].cells[col].firstElementChild.src.includes(str) && board.rows[1].cells[col].firstElementChild.src.includes(str) && board.rows[2].cells[col].firstElementChild.src.includes(str));
}

function checkDiagonal1(str) {
    let contador = 0;
    let buttonsInRow = document.getElementsByClassName("buttonImg");

    for (let item of buttonsInRow) {
        if ((item.classList.contains("row0") && item.classList.contains("col0")) || (item.classList.contains("row1") && item.classList.contains("col1")) || (item.classList.contains("row2") && item.classList.contains("col2"))) {
            if (item.firstElementChild.src.includes(str)) {
                contador++;
            }
        }
    }

    return (contador === 3);
    //return (board.rows[0].cells[0].firstElementChild.src.includes(str) && board.rows[1].cells[1].firstElementChild.src.includes(str) && board.rows[2].cells[2].firstElementChild.src.includes(str));
}

function checkDiagonal2(str) {
    let contador = 0;
    let buttonsInRow = document.getElementsByClassName("buttonImg");

    for (let item of buttonsInRow) {
        if ((item.classList.contains("row0") && item.classList.contains("col2")) || (item.classList.contains("row1") && item.classList.contains("col1")) || (item.classList.contains("row2") && item.classList.contains("col0"))) {
            if (item.firstElementChild.src.includes(str)) {
                contador++;
            }
        }
    }

    return (contador === 3);
    //return (board.rows[0].cells[2].firstElementChild.src.includes(str) && board.rows[1].cells[1].firstElementChild.src.includes(str) && board.rows[2].cells[0].firstElementChild.src.includes(str));
}