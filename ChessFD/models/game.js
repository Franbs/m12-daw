const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const gameSchema = new Schema(
    {
        player1: {
            type: String
        },
        player2: {
            type: String
        },
        finished: {
            type: Boolean
        },
        row: {
            type: Number
        },
        col: {
            type: Number
        }
    },
    {
        timestamps: true
});

module.exports = mongoose.model('Game', gameSchema);